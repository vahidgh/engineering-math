# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi

##### Solutions by Vahid Ghayoomie


# **Contents**

* [Chapter 1 -- `SPECIAL FUNCTIONS`](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter1.ipynb)
  * [Chapter1.md (text file)](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter1.md)
* [Chapter 2 -- `FOURIER INTEGRALS AND FOURIER TRANSFORMS`](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter2.ipynb)
  * [Chapter2.md (text file)](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter2.md)
* [Chapter 3 -- `APPLICATIONS INVOLVING FOURIER TRANSFORMS`](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter3.ipynb)
  * [Chapter3.md (text file)](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter3.md)
* [Chapter 4 -- `THE LAPLACE TRANSFORM`](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter4.ipynb)
  * [Chapter4.md (text file)](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter4.md)
* [Chapter 5 -- `APPLICATIONS INVOLVING LAPLACE TRANSFORMS`](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter5.ipynb)
  * [Chapter5.md (text file)](https://gitlab.com/vahidgh/engineering-math/-/blob/master/chapter5.md)
