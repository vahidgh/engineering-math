# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi
  
##### Solutions by Vahid Ghayoomie


# **Chapter 2**

### *FOURIER INTEGRALS AND FOURIER TRANSFORMS*

----

##### EXERCISES 2.2

**3. Find an integral representation for**  

```math 
f(x)= 
\begin{cases}
    1-x^2, & |x|<1 
    \\0, & |x|>1 
\end{cases}
```  

**And deduce the value of the integral**

```math 
I=\int_0^\infty (\frac{\sin x - x \cos x}{x^3}) \cos \frac{1}{2}x \quad dx 
```

---

**Solution:**

$` A(s)=\frac{\int\limits_{-1}^{1} \left(1 - t^{2}\right) \cos{\left(s t \right)}\, dt}{\pi}=\begin{cases} \frac{4 \left(- s \cos{\left(s \right)} + \sin{\left(s \right)}\right)}{\pi s^{3}} & \text{for}\: s > -\infty \wedge s < \infty \wedge s \neq 0 \\\frac{4}{3 \pi} & \text{otherwise} \end{cases}`$ 
$` B(s)=\frac{\int\limits_{-1}^{1} \left(1 - t^{2}\right) \sin{\left(s t \right)}\, dt}{\pi}=0`$ 

$` So \quad we \quad can \quad write:`$ 

$` f(x)=\int\limits_{0}^{\infty} \frac{4 \left(- s \cos{\left(s \right)} + \sin{\left(s \right)}\right) \cos{\left(s x \right)}}{\pi s^{3}}\, ds`$ 

$` Since \quad f(1/2)=0.75`$ 

$` 0.75=\int\limits_{0}^{\infty} \frac{4 \left(- s \cos{\left(s \right)} + \sin{\left(s \right)}\right) \cos{\left(0.5 s \right)}}{\pi s^{3}}\, ds`$ 

$` And \quad replacing \quad s \quad with \quad x:`$ 

$` I=0.1875 \pi`$ 

----

##### EXERCISES 2.4

**15. Use the result of Exam.2.6 with $` a = (1-i)/2 `$ to derive**

(a) $` \mathcal{F\{\cos(\frac{t^2}{2}); s\}} = \frac{1}{\sqrt 2}[\cos(\frac{s^2}{2})+\sin(\frac{s^2}{2})] `$ 

(b) $` \mathcal{F\{\sin(\frac{t^2}{2}); s\}} = \frac{1}{\sqrt 2}[\cos(\frac{s^2}{2})-\sin(\frac{s^2}{2})] `$ 

(c) $` \mathcal{F_s\{\frac{1}{t}\cos(\frac{t^2}{2}); s\}} = \sqrt \frac{\pi}{2}[C(\frac{s}{\sqrt \pi})+S(\frac{s}{\sqrt \pi})] `$ 

---

**Solution:**

$` Knowing \quad that:`$ 

$` \left(\frac{1}{2} - \frac{i}{2}\right)^{2}=- \frac{i}{2}`$ 

$` \mathcal{F}_{t}\left[e^{\frac{i t^{2}}{2}}\right]\left(s\right)=\sqrt{2} \left(\frac{1}{2} + \frac{i}{2}\right) e^{- \frac{i s^{2}}{2}}`$ 

$` Separating \quad imaginary \quad and \quad real \quad parts:`$ 

$` \mathcal{F}_{t}\left[i \sin{\left(\frac{t^{2}}{2} \right)} + \cos{\left(\frac{t^{2}}{2} \right)}\right]\left(s\right)=\sqrt{2} \left(\frac{1}{2} + \frac{i}{2}\right) \left(- i \sin{\left(\frac{s^{2}}{2} \right)} + \cos{\left(\frac{s^{2}}{2} \right)}\right)`$ 

$` \mathcal{F}_{t}\left[i \sin{\left(\frac{t^{2}}{2} \right)} + \cos{\left(\frac{t^{2}}{2} \right)}\right]\left(s\right)=\frac{\sqrt{2} \sin{\left(\frac{s^{2}}{2} \right)}}{2} - \frac{\sqrt{2} i \sin{\left(\frac{s^{2}}{2} \right)}}{2} + \frac{\sqrt{2} \cos{\left(\frac{s^{2}}{2} \right)}}{2} + \frac{\sqrt{2} i \cos{\left(\frac{s^{2}}{2} \right)}}{2}`$ 

$` \mathcal{F}_{t}\left[\cos{\left(\frac{t^{2}}{2} \right)}\right]\left(s\right)=\frac{\sqrt{2} \left(\sin{\left(\frac{s^{2}}{2} \right)} + \cos{\left(\frac{s^{2}}{2} \right)}\right)}{2}`$ 

$` \mathcal{F}_{t}\left[\sin{\left(\frac{t^{2}}{2} \right)}\right]\left(s\right)=\frac{\sqrt{2} \left(- \sin{\left(\frac{s^{2}}{2} \right)} + \cos{\left(\frac{s^{2}}{2} \right)}\right)}{2}`$ 


$` For \quad part \quad (c):`$ 

$` Since \quad cos(t^2/2) \quad is \quad an \quad even \quad function:`$ 

$` \mathcal{F}_{t}\left[\cos{\left(\frac{t^{2}}{2} \right)}\right]\left(s\right)=\frac{\sqrt{2} \int\limits_{0}^{\infty} \cos{\left(\frac{t^{2}}{2} \right)} \cos{\left(s t \right)}\, dt}{\sqrt{\pi}}=\frac{\sqrt{2} \left(\sin{\left(\frac{s^{2}}{2} \right)} + \cos{\left(\frac{s^{2}}{2} \right)}\right)}{2}`$ 

$` Integrating \quad both \quad parts \quad with \quad respect \quad to \quad s:`$ 

$` \frac{\sqrt{2} \int\limits_{0}^{s} \cos{\left(\frac{t^{2}}{2} \right)} \int\limits_{0}^{\infty} \cos{\left(s t \right)}\, ds\, dt}{\sqrt{\pi}}=\int\limits_{0}^{s} \frac{\sqrt{2} \left(\sin{\left(\frac{s^{2}}{2} \right)} + \cos{\left(\frac{s^{2}}{2} \right)}\right)}{2}\, ds`$ 

$` Replacing \quad s \quad with \quad s\sqrt \pi \quad and \quad knowing \quad that:`$ 

$` C\left(\frac{s}{\sqrt{\pi}}\right)=\int\limits_{0}^{\frac{s}{\sqrt{\pi}}} \cos{\left(\frac{\pi t^{2}}{2} \right)}\, dt`$ 

$` S\left(\frac{s}{\sqrt{\pi}}\right)=\int\limits_{0}^{\frac{s}{\sqrt{\pi}}} \sin{\left(\frac{\pi t^{2}}{2} \right)}\, dt`$ 

$` \frac{\sqrt{2} \int\limits_{0}^{\infty} \frac{\sin{\left(s t \right)} \cos{\left(\frac{t^{2}}{2} \right)}}{t}\, dt}{\sqrt{\pi}}=\int\limits_{0}^{\frac{s}{\sqrt{\pi}}} \frac{\sqrt{2} \sqrt{\pi} \left(\sin{\left(\frac{\pi s^{2}}{2} \right)} + \cos{\left(\frac{\pi s^{2}}{2} \right)}\right)}{2}\, ds`$ 

$` \mathcal{SIN}_{s}\left[\frac{\cos{\left(\frac{t^{2}}{2} \right)}}{t}\right]\left(t\right)=\frac{\sqrt{2} \sqrt{\pi} \left(C\left(\frac{s}{\sqrt{\pi}}\right) + S\left(\frac{s}{\sqrt{\pi}}\right)\right)}{2}`$ 

---

##### EXERCISES 2.5

**6. Show that**

(a) $` \mathcal{F_c\{f(t)\sin(at); s\}} = \frac{1}{2}[F_s(s+a) - F_s(s-a)] `$ 

(b) $` \mathcal{F_s\{f(t)\sin(at); s\}} = \frac{1}{2}[F_c(s-a) - F_c(s+a)] `$ 

---

**Solution:**

$` \mathcal{COS}_{t}\left[f{\left(t \right)} \sin{\left(a t \right)}\right]\left(s\right)=\int\limits_{0}^{\infty} f{\left(t \right)} \sin{\left(a t \right)} \cos{\left(s t \right)}\, dt=\int\limits_{0}^{\infty} \frac{\left(\sin{\left(a t - s t \right)} + \sin{\left(a t + s t \right)}\right) f{\left(t \right)}}{2}\, dt=\int\limits_{0}^{\infty} \frac{f{\left(t \right)} \sin{\left(a t - s t \right)}}{2}\, dt + \int\limits_{0}^{\infty} \frac{f{\left(t \right)} \sin{\left(a t + s t \right)}}{2}\, dt=0.5 \mathcal{SIN}_{t}\left[a + s\right]\left(s\right)-0.5 \mathcal{SIN}_{t}\left[- a + s\right]\left(s\right)`$ 

$` \mathcal{SIN}_{t}\left[f{\left(t \right)} \sin{\left(a t \right)}\right]\left(s\right)=\int\limits_{0}^{\infty} f{\left(t \right)} \sin{\left(a t \right)} \sin{\left(s t \right)}\, dt=\int\limits_{0}^{\infty} \frac{\left(\cos{\left(a t - s t \right)} - \cos{\left(a t + s t \right)}\right) f{\left(t \right)}}{2}\, dt=\int\limits_{0}^{\infty} \frac{f{\left(t \right)} \cos{\left(a t - s t \right)}}{2}\, dt + \int\limits_{0}^{\infty} \left(- \frac{f{\left(t \right)} \cos{\left(a t + s t \right)}}{2}\right)\, dt=0.5 \mathcal{COS}_{t}\left[- a + s\right]\left(s\right)-0.5 \mathcal{COS}_{t}\left[a + s\right]\left(s\right)`$ 

---

##### EXERCISES 2.6

**5. Integrate both sides of Eq. (2.67) with respect to s to deduce the Fourier sine transform of $` (1/t)J_0(at) `$.**

---

**Solution:**

$` \mathcal{COS}_{t}\left[J_{0}\left(a t\right)\right]\left(s\right)=\begin{cases} \frac{\sqrt{2}}{\sqrt{\pi} \sqrt{a^{2} - s^{2}}} & \text{for}\: a > \left|{s}\right| \\0 & \text{for}\: a < \left|{s}\right| \end{cases}`$ 

$` \int\limits_{0}^{s}\int\limits_{0}^{\infty} \frac{\sqrt{2} \cos{\left(s t \right)} J_{0}\left(a t\right)}{\sqrt{\pi}}\, dt\, ds=\int\limits_{s}^{\infty} \frac{\sqrt{2}}{\sqrt{\pi} \sqrt{a^{2} - s^{2}}}\, ds`$ 

$` \int\limits_{0}^{\infty} \frac{\sqrt{2} \sin{\left(s t \right)} J_{0}\left(a t\right)}{\sqrt{\pi} t}\, dt=\frac{\sqrt{2} \operatorname{asin}{\left(\frac{s}{a} \right)}}{\sqrt{\pi}}`$ 

$` \mathcal{SIN}_{t}\left[\frac{J_{0}\left(a t\right)}{t}\right]\left(s\right)=\operatorname{asin}{\left(\frac{s}{a} \right)}`$ 

---

**In Probs. 15-17, use residue theory to find the inverse Fourier transform of the given rational function.**

16.  $` F(s) = \frac{2is+1}{s^2+1} `$ 

---

**Solution:**


$` \mathtt{\text{F(s)}}=\frac{2 i s + 1}{s^{2} + 1}`$ 

$` Replacing \quad s \quad with \quad z:`$ 

$` \mathtt{\text{F(z)}}=\frac{2 i z + 1}{z^{2} + 1}`$ 

$` Which \quad has \quad poles \quad at:`$ 
$` \left[ - i, \  i\right]`$ 

$` Calculating \quad residues \quad at \quad poles:`$ 

$` Res\{F(z)e^{itz};z_i\}=\lim_{z \to x^+}\left(f(t) \left(- x + z\right) e^{- i t z}\right)`$ 

$` Res\{F(z)e^{itz};z_1\}=\frac{3 i e^{- t}}{2}`$ 

$` Res\{F(z)e^{itz};z_2\}=\frac{i e^{t}}{2}`$ 

$` And \quad the \quad Inverse \quad Fourier \quad Transform \quad is:\sqrt{2} i \sqrt{\pi} \sum_{n=1}^{2} \left[\begin{matrix}\frac{3 i e^{- t}}{2} & \frac{i e^{t}}{2}\end{matrix}\right]`$ 

$` \mathcal{F^{-1}\{\frac{2is+1}{s^2+1}; t\}}=- \frac{\sqrt{2} \sqrt{\pi} \left(e^{2 t} + 3\right) e^{- t}}{2}`$ 

---

##### EXERCISES 2.7

**5. Use the Fourier transform relation.** 

```math 
\mathcal{F\{(a^2-t^2)^{\frac{-1}{2}}h(a-|t|);s\}} = \sqrt\frac{\pi}{2}J_0(as), \quad a>0 
``` 

**to show that**  

```math 
\int_0^\infty J_0(ax)J_0(bx) dx = \frac{2}{\pi b}K(\frac{a}{b}), \quad 0<a<b 
``` 
 
**where K(m) denotes the complete elliptic integral** 

```math 
K(m) = \int_0^{\frac{\pi}{2}}(1-m^2 sin^2\theta)^{\frac{-1}{2}} d\theta 
```

 ---

**Solution:**

$` \mathcal{F}_{t}\left[\frac{h{\left(a - \left|{t}\right| \right)}}{\left(a^{2} - t^{2}\right)^{0.5}}\right]\left(s\right)=\frac{\sqrt{2} \sqrt{\pi} j_{0}\left(a s\right)}{2}`$ 

$` Considering:`$ 

$` F(x)=j_{0}\left(a x\right)`$ 

$` G(x)=j_{0}\left(b x\right)`$ 

$` \int\limits_{0}^{\infty} j_{0}\left(a x\right) j_{0}\left(b x\right)\, dx=\int\limits_{0}^{\infty} F(x) G(x)\, dx=\int\limits_{0}^{\infty} f(t) g(-t)\, dt`$ 

$` Knowing \quad that:`$ 

$` \mathcal{F}^{-1}_{s}\left[j_{0}\left(a s\right)\right]\left(t\right)=\frac{\sqrt{2} h{\left(a - \left|{t}\right| \right)}}{\sqrt{\pi} \left(a^{2} - t^{2}\right)^{0.5}}`$ 

$` We \quad can \quad write:`$ 

$` \int\limits_{0}^{\infty} j_{0}\left(a x\right) j_{0}\left(b x\right)\, dx=\int\limits_{0}^{\infty} \frac{2 h{\left(a - \left|{t}\right| \right)} h{\left(b - \left|{t}\right| \right)}}{\pi \left(a^{2} - t^{2}\right)^{0.5} \left(b^{2} - t^{2}\right)^{0.5}}\, dt`$ 

$` Replacing \quad t \quad with \quad a\,\sin(\theta) \quad and \quad considering \quad 0<a<b:`$ 

$` \int\limits_{0}^{\infty} \frac{2 h{\left(a - \left|{t}\right| \right)} h{\left(b - \left|{t}\right| \right)}}{\pi \left(a^{2} - t^{2}\right)^{0.5} \left(b^{2} - t^{2}\right)^{0.5}}\, dt=\int\limits_{0}^{\frac{\pi}{2}} \frac{2 a \cos{\left(\theta \right)}}{\pi \left(- a^{2} \sin^{2}{\left(\theta \right)} + a^{2}\right)^{0.5} \left(- a^{2} \sin^{2}{\left(\theta \right)} + b^{2}\right)^{0.5}}\, d\theta=\frac{2 \int\limits_{0}^{\frac{\pi}{2}} \frac{1}{\sqrt{- \frac{a^{2} \sin^{2}{\left(\theta \right)}}{b^{2}} + 1}}\, d\theta}{\pi b}`$ 

