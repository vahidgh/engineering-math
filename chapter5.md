# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi
  
##### Solutions by Vahid Ghayoomie

# **Chapter 5**

### *APPLICATIONS INVOLVING LAPLACE TRANSFORMS*

---

##### EXERCISES 5.2

**In Probs. 1-10, use known Laplace transforms or transform properties to evaluate the given integral.**

$` 7. \quad \int_{0}^{\infty}{\frac{\cos(6t)-\cos(4t)}{t}\, dx} `$ 
 
$` 10. \quad \int_{0}^{\infty}{xe^{-x^2}erfc(x)\, dx} `$ 
 
---

**Solution:**

7.  

$` By \quad converting \quad Cosine \quad to \quad Sine \quad we \quad can \quad write:`$ 
 
$` - \cos{\left(4 t \right)} + \cos{\left(6 t \right)}=- 2 \sin{\left(t \right)} \sin{\left(5 t \right)}`$ 
 
$` Now \quad converting \quad sin(5t) \quad to \quad exponential \quad form:`$ 
 
$` - 2 \sin{\left(t \right)} \sin{\left(5 t \right)}=i e^{5 i t} \sin{\left(t \right)} - i e^{- 5 i t} \sin{\left(t \right)}`$ 
 
$` So, \quad we \quad can \quad write:`$ 
 
$` \int\limits_{0}^{\infty} \frac{- \cos{\left(4 t \right)} + \cos{\left(6 t \right)}}{t}\, dt=\int\limits_{0}^{\infty} \frac{i e^{5 i t} \sin{\left(t \right)}}{t}\, dt + \int\limits_{0}^{\infty} \left(- \frac{i e^{- 5 i t} \sin{\left(t \right)}}{t}\right)\, dt`$ 
 
$` Which \quad are \quad equal \quad to:`$ 
 
$` \int\limits_{0}^{\infty} \frac{- \cos{\left(4 t \right)} + \cos{\left(6 t \right)}}{t}\, dt=\mathcal{L}_{t}\left[\frac{i \sin{\left(t \right)}}{t}\right]\left(\left( p, \  - 5 i\right)\right) - \mathcal{L}_{t}\left[\frac{i \sin{\left(t \right)}}{t}\right]\left(\left( p, \  5 i\right)\right)`$ 
 
$` \int\limits_{0}^{\infty} \frac{- \cos{\left(4 t \right)} + \cos{\left(6 t \right)}}{t}\, dt=- i \left(\frac{\pi}{2} - i \operatorname{atanh}{\left(5 \right)}\right) + i \left(\frac{\pi}{2} + i \operatorname{atanh}{\left(5 \right)}\right)=- 2 \operatorname{atanh}{\left(5 \right)}`$ 
 
---

**Solution:**

10. 

$` By \quad replacing \quad x \quad with \quad \sqrt{pt} \quad we \quad can \quad write:`$ 
 
$` \int\limits_{0}^{\infty} x e^{- x^{2}} \operatorname{erfc}{\left(x \right)}\, dx=\int\limits_{0}^{\frac{\infty}{p}} \frac{p e^{- p t} \operatorname{erfc}{\left(\sqrt{p t} \right)}}{2}\, dt`$ 
 
$` From \quad Laplace \quad properties \quad we \quad know:`$ 
 
$` \mathcal{L}_{t}\left[\operatorname{erfc}{\left(\sqrt{p t} \right)}\right]\left(p\right)=\int\limits_{0}^{\infty} e^{- p t} \operatorname{erfc}{\left(\sqrt{p t} \right)}\, dt=\frac{2 - \sqrt{2}}{2 p}`$ 
 
$` So, \quad we \quad deduce:`$ 
 
$` \int\limits_{0}^{\infty} x e^{- x^{2}} \operatorname{erfc}{\left(x \right)}\, dx=\frac{1}{2} - \frac{\sqrt{2}}{4}`$ 
 
---

##### EXERCISES 5.3

**In Probs. 1-15, use the Laplace transform to solve the given initial value problem.**

$` 6. \quad y^{\prime \prime \prime} - 3y^{\prime \prime} + 3y^{\prime} - y = t^2 e^t, \quad y(0)=1, \, y^{\prime}(0)=0, \, y^{\prime \prime}(0)=-2 `$ 
 
---

**Solution:**

$` From \quad Laplace \quad properties \quad we \quad know:`$ 
 
$` \mathcal{L}_{t}\left[- y{\left(t \right)} + 3 \frac{d}{d t} y{\left(t \right)} - 3 \frac{d^{2}}{d t^{2}} y{\left(t \right)} + \frac{d^{3}}{d t^{3}} y{\left(t \right)}\right]\left(p\right) = - \mathcal{L}_{t}\left[y{\left(t \right)}\right]\left(p\right) + 3 \mathcal{L}_{t}\left[\frac{d}{d t} y{\left(t \right)}\right]\left(p\right) - 3 \mathcal{L}_{t}\left[\frac{d^{2}}{d t^{2}} y{\left(t \right)}\right]\left(p\right) + \mathcal{L}_{t}\left[\frac{d^{3}}{d t^{3}} y{\left(t \right)}\right]\left(p\right)`$ 
 
$` \mathcal{L}_{t}\left[\frac{d^{3}}{d t^{3}} y{\left(t \right)}\right]\left(p\right) = p^{3} Y{\left(p \right)} - p^{2} y{\left(0 \right)} - p \frac{d}{d t} y{\left(t \right)} - \frac{d^{2}}{d t^{2}} y{\left(t \right)}`$ 
 
$` \mathcal{L}_{t}\left[\frac{d^{2}}{d t^{2}} y{\left(t \right)}\right]\left(p\right) = p^{2} Y{\left(p \right)} - p y{\left(0 \right)} - \frac{d}{d t} y{\left(t \right)}`$ 
 
$` \mathcal{L}_{t}\left[\frac{d}{d t} y{\left(t \right)}\right]\left(p\right) = p Y{\left(p \right)} - y{\left(0 \right)}`$ 
 
$` \mathcal{L}_{t}\left[y{\left(t \right)}\right]\left(p\right) = Y{\left(p \right)}`$ 
 
$` \mathcal{L}_{t}\left[t^{2} e^{t}\right]\left(p\right) = \frac{2}{\left(p - 1\right)^{3}}`$ 
 
$` p^{3} Y{\left(p \right)} - 3 p^{2} Y{\left(p \right)} - p^{2} y{\left(0 \right)} + 3 p Y{\left(p \right)} + 3 p y{\left(0 \right)} - p \frac{d}{d t} y{\left(0 \right)} - Y{\left(p \right)} - 3 y{\left(0 \right)} + 3 \frac{d}{d t} y{\left(0 \right)} - \frac{d^{2}}{d t^{2}} y{\left(0 \right)} = p^{3} Y{\left(p \right)} - 3 p^{2} Y{\left(p \right)} - p^{2} + 3 p Y{\left(p \right)} + 3 p - Y{\left(p \right)} - 1 = \frac{2}{\left(p - 1\right)^{3}}`$ 
 
$` Y{\left(p \right)} = \frac{p^{2} \left(p - 1\right)^{3} - 3 p \left(p - 1\right)^{3} + \left(p - 1\right)^{3} + 2}{\left(p - 1\right)^{3} \left(p^{3} - 3 p^{2} + 3 p - 1\right)} = \frac{p^{5} - 6 p^{4} + 13 p^{3} - 13 p^{2} + 6 p + 1}{\left(p - 1\right)^{6}}`$ 
 

$` Taking \quad Inverse \quad Laplace \quad from \quad each \quad part:`$ 
 
$` \mathcal{L}^{-1}_{p}\left[\frac{p^{2}}{\left(p - 1\right)^{3}}\right]\left(t\right) = \frac{\left(t^{2} + 4 t + 2\right) e^{t}}{2}`$ 
 
$` \mathcal{L}^{-1}_{p}\left[- \frac{3 p}{\left(p - 1\right)^{3}}\right]\left(t\right) = - \frac{3 t \left(t + 2\right) e^{t}}{2}`$ 
 
$` \mathcal{L}^{-1}_{p}\left[\frac{1}{\left(p - 1\right)^{3}}\right]\left(t\right) = \frac{t^{2} e^{t}}{2}`$ 
 
$` \mathcal{L}^{-1}_{p}\left[\frac{2}{\left(p - 1\right)^{6}}\right]\left(t\right) = \frac{t^{5} e^{t}}{60}`$ 
 

$` So, \quad we \quad deduce:`$ 
 
$` y{\left(t \right)} = \left(t \left(t \left(\frac{t^{3}}{60} - \frac{1}{2}\right) - 1\right) + 1\right) e^{t}`$ 
 
---

**In Probs. 16-21, use the Laplace transform to construct the impulse response function and the one-sided Green's function for the given differential operator M, where D = d/dt.**

$` 17. \quad M = (D-a)(D-b), \quad a \neq b `$ 
 
---

**Solution:**

$` Using \quad Eq.(5.8), \quad the \quad impulse \quad response \quad function \quad is:`$ 
 
$` g(t) = \mathcal{L}^{-1}_{p}\left[\frac{1}{\left(- a + p\right) \left(- b + p\right)}\right]\left(t\right) = \mathcal{L}^{-1}_{p}\left[- \frac{1}{\left(a - b\right) \left(- b + p\right)} + \frac{1}{\left(- a + p\right) \left(a - b\right)}\right]\left(t\right)`$ 
 
$` g(t) = \frac{e^{a t} - e^{b t}}{a - b}`$ 
 

$` So, \quad the \quad one-sided \quad Green's \quad function \quad is:`$ 
 
$` g(t-u) = \frac{e^{a \left(t - u\right)} - e^{b \left(t - u\right)}}{a - b}`$ 
 
---

##### EXERCISES 5.4  

**11. Given the boundary-value problem**
```math
 u_{xx}=u_t, \quad 0<x<1, \, t>0 
```
```math
 B.C.: \quad u(0,t)=0, \quad u(1,t)=T_0 
```
```math
 I.C.: \quad u(x,0)=T_0, \quad 0<x<1 
```

**(a) Show that the solution of the transformed problem can be expressed in the form**  

```math
 U(x,p)=\frac{T_0}{p}\left(1 - e^{-\sqrt{p}} \left[\frac{1 - e^{-2(1-x)\sqrt{p}}}{1 - e^{-2\sqrt{p}}}\right]\right) 
```
**(b) By expanding $` (1 - e^{-2\sqrt{p}})^{-1} `$ in a series of ascending powers of $` e^{-2\sqrt{p}} `$, show that** 

```math
 U(x,p)=\frac{T_0}{p}\left[1 - e^{-x\sqrt{p}} + e^{-(2-x)\sqrt{p}} - e^{-(2+x)\sqrt{p}} + ... \right] 
```
**(c) Inverting the series in (b) termwise, deduce that** 

```math
 u(x,t)= T_0 \{ erf(x/2\sqrt{t}) + erfc[(2-x)/2\sqrt{t}] - erfc[(2+x)/2\sqrt{t}] + ... \} 
```

---

**Solution:**

$` From \quad Laplace \quad properties \quad we \quad know:`$ 
 
$` U_t(x,p)=p U{\left(x,p \right)} - u{\left(x,0 \right)}`$ 
 
$` U(0,p)=0, \quad U(1,p)=T_0/p`$ 
 
$` Using \quad (5.19) \quad we \quad can \quad write:`$ 
 
$` T_{0} - p U{\left(x,p \right)} + \frac{\partial^{2}}{\partial x^{2}} U{\left(x,p \right)} = 0, \quad 0<x<1`$ 
 
$` Which \quad has \quad the \quad solution:`$ 
 
$` U(x,p)=C_{1} e^{- \sqrt{p} x} + C_{2} e^{\sqrt{p} x} + \frac{T_{0}}{p}`$ 
 
$` And \quad by \quad applying \quad B.C.:`$ 
 
$` U(x,p)=- \frac{T_{0} e^{2 \sqrt{p}} e^{- \sqrt{p} x}}{p e^{2 \sqrt{p}} - p} + \frac{T_{0} e^{\sqrt{p} x}}{p e^{2 \sqrt{p}} - p} + \frac{T_{0}}{p}=- \frac{T_{0} \left(- \left(e^{2 \sqrt{p}} - 1\right) e^{\sqrt{p} x} + e^{2 \sqrt{p}} - e^{2 \sqrt{p} x}\right) e^{- \sqrt{p} x}}{p \left(e^{2 \sqrt{p}} - 1\right)}`$ 
 
$` U(x,p)=\frac{T_{0} \left(1 - e^{- \sqrt{p}}\right) \left(1 - e^{\sqrt{p} \left(2 x - 2\right)}\right)}{p \left(1 - e^{- 2 \sqrt{p}}\right)}`$ 
 

$` Knowing \quad that:`$ 
 
$` \frac{1}{1 - e^{- 2 \sqrt{p}}}=\sum_{k=0}^{\infty} \left(e^{- 2 \sqrt{p}}\right)^{k}=1 + e^{- 2 \sqrt{p}} + e^{- 4 \sqrt{p}} + e^{- 6 \sqrt{p}} + ...`$ 
 
$` U(x,p)=\frac{T_{0} \left(- e^{\sqrt{p} \left(- x - 2\right)} + e^{\sqrt{p} \left(x - 2\right)} + 1 - e^{- \sqrt{p} x}\right)}{p} + ...`$ 
 

$` Finally \quad by \quad taking \quad Inverse \quad Laplace \quad Transform \quad we \quad have:`$ 
 
$` u(x,t)=\mathcal{L}^{-1}_{p}\left[\frac{T_{0} \left(- e^{\sqrt{p} \left(- x - 2\right)} + e^{\sqrt{p} \left(x - 2\right)} + 1 - e^{- \sqrt{p} x}\right)}{p}\right]\left(t\right) + ... =T_{0} \left(\operatorname{erf}{\left(\frac{x}{2 \sqrt{t}} \right)} - 1\right) + T_{0} \left(\operatorname{erf}{\left(\frac{x - 2}{2 \sqrt{t}} \right)} + 1\right) + T_{0} \left(\operatorname{erf}{\left(\frac{x + 2}{2 \sqrt{t}} \right)} - 1\right) + T_{0} + ...`$ 
 
---

**In Probs. 19-25, use the Laplace transform to solve the given boundary-value problem.** 

22. 
```math
 u_{xx} = c^{-2} u_{tt}, \quad 0<x<1, \, t>0 
```
```math
 B.C.: \quad u(0,t)=0, \quad u(1,t)=1 
```
```math
 I.C.: \quad u(x,0)=0, \quad u_t(x,0)=0 
```
---

**Solution:**

$` From \quad Laplace \quad properties \quad we \quad know:`$ 
 
$` U_{tt}(x,p)=p^{2} U{\left(x,p \right)} - p u{\left(x,0 \right)} - \operatorname{u_{t}}{\left(x,0 \right)}`$ 
 
$` U(0,p)=0, \quad U(1,p)=1/p`$ 
 
$` Using \quad (5.19) \quad and \quad (5.24) \quad we \quad can \quad write:`$ 
 
$` \frac{\partial^{2}}{\partial x^{2}} U{\left(x,p \right)} - \frac{p^{2} U{\left(x,p \right)}}{c^{2}} = 0, \quad 0<x<1`$ 
 
$` Which \quad has \quad the \quad solution:`$ 
 
$` U(x,p)=C_{1} \cosh{\left(\frac{p x}{c} \right)} + C_{2} \sinh{\left(\frac{p x}{c} \right)}`$ 
 
$` And \quad by \quad applying \quad B.C.:`$ 
 
$` U(x,p)=\frac{\sinh{\left(\frac{p x}{c} \right)}}{p \sinh{\left(\frac{p}{c} \right)}}`$ 
 
$` Finally \quad by \quad using \quad (5.23) \quad we \quad have:`$ 
 
$` u(x,t) = x + \frac{2 \sum_{n=1}^{\infty} \frac{\left(-1\right)^{n} e^{- c^{2} n^{2} p^{2} t^{2}} \sin{\left(\pi n x \right)}}{n}}{\pi}`$ 
 
---

##### EXERCISES 5.5  

**16. Show that**

```math
 u(t) + \int_{0}^{t}{u(\tau) \sin{\, \omega(t-\tau)} \, d\tau}=f(t) ```math
  

**has the formal solution**  
```math
 u(t) = f(t) - \frac{\omega}{\Omega} \int_{0}^{t}{f(\tau) \sin{\, \Omega(t-\tau)} \, d\tau}, \quad \Omega^2=\omega(1+\omega) 
```

---

**Solution:**

$` By \quad taking \quad Laplace \quad Transform \quad of \quad each \quad term \quad we \quad have:`$ 
 
$` \mathcal{L}_{t}\left[\sin{\left(\omega t \right)}\right]\left(p\right)=\frac{\omega}{\omega^{2} + p^{2}}`$ 
 
$` \frac{\omega U{\left(p \right)}}{\omega^{2} + p^{2}} + U{\left(p \right)} = F{\left(p \right)}`$ 
 
$` U(p) = \frac{\left(\omega^{2} + p^{2}\right) F{\left(p \right)}}{\omega^{2} + \omega + p^{2}} = - \frac{\omega F{\left(p \right)}}{\omega^{2} + \omega + p^{2}} + F{\left(p \right)} = - \frac{\omega F{\left(p \right)}}{\Omega^{2} + p^{2}} + F{\left(p \right)}`$ 
 

$` Now \quad by \quad taking \quad Inverse \quad Laplace \quad Transform \quad we \quad have:`$ 
 
$` \mathcal{L}^{-1}_{p}\left[\frac{\omega}{\Omega^{2} + p^{2}}\right]\left(t\right)=\frac{\omega \sin{\left(\Omega t \right)}}{\Omega}`$ 
 
$` So, \quad by \quad Convolution \quad theorem \quad we \quad conclude:`$ 
 
$` u(t) = f{\left(t \right)} - \frac{\omega \int\limits_{0}^{t} f{\left(\tau \right)} \sin{\left(\Omega \left(t - \tau\right) \right)}\, d\tau}{\Omega}`$ 
 
