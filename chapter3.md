# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi
  
##### Solutions by Vahid Ghayoomie

# **Chapter 3**

### *APPLICATIONS INVOLVING FOURIER TRANSFORMS*

---

##### EXERCISES 3.2  

**1. Given the integral**  

```math 
 I=\int_{x-1}^{x+1}e^{-|u|}du 
 ``` 
  

**Show that** 

$` (a) \quad I=\int_{x-1}^{x+1}e^udu, \quad -\infty<x<-1 `$  

$` (b) \quad I=\int_{x-1}^{0}e^udu + \int_{0}^{x+1}e^{-u}du, \quad -1\leq x\leq 1 `$  

$` (c) \quad I=\int_{x-1}^{x+1}e^{-u}du, \quad 1<x<\infty `$  

$` (d) \quad From \quad (a), \quad (b), \quad and \quad (c), \quad deduce \quad the \quad value \quad of \quad I. `$ 

---

**Solution:**

$` \mathtt{\text{I}}=\int\limits_{x - 1}^{x + 1} e^{- \left|{u}\right|}\, du=\int\limits_{-\infty}^{\infty} h{\left(1 - \left|{u - x}\right| \right)} e^{- \left|{u}\right|}\, du`$ 

u is always negative if x<-1 or -1<x<0 and x-1<u<0, and it is always positive if x>1 or 0>x>1 and 0<u<x+1, so we can write:

$` f(u(x))=\begin{cases} \int\limits_{x - 1}^{x + 1} e^{u}\, du & \text{for}\: x \geq -\infty \wedge x < -1 \int\limits_{0}^{x + 1} e^{- u}\, du + \int\limits_{x - 1}^{0} e^{u}\, du & \text{for}\: x \geq -1 \wedge x \leq 1 \int\limits_{x - 1}^{x + 1} e^{- u}\, du & \text{for}\: x > 1 \wedge x < \infty \end{cases}`$ 

$` I=\begin{cases} - e^{x - 1} + e^{x + 1} & \text{for}\: x \geq -\infty \wedge x < -1 \\- e^{- x - 1} - e^{x - 1} + 2 & \text{for}\: x \geq -1 \wedge x \leq 1 \\e^{1 - x} - e^{- x - 1} & \text{for}\: x > 1 \wedge x < \infty \end{cases}`$ 

---

##### EXERCISES 3.3  

**In Probs. 1-5, solve the heat conduction problem described by (3.26) when the initial temperature distribution f(x) is prescribed as given.** 

```math 
 4. \quad f(x)=
\begin{cases}
    0, & x<0 
    \\ e^{-x}, & x>0 
\end{cases} 
 ``` 

---

**Solution:**

$` f(x)=\begin{cases} e^{- x} & \text{for}\: x > 0 \\0 & \text{for}\: x < 0 \end{cases}`$ 

$` Knowing \quad that:`$ 

$` u(x,t)=\frac{\int\limits_{-\infty}^{\infty} f{\left(\zeta \right)} e^{- \frac{\left(x - \zeta\right)^{2}}{4 a^{2} t}}\, d\zeta}{2 \sqrt{\pi} a \sqrt{t}}`$

$` Replacing \quad f(\zeta) \quad with \quad f(x):`$

$` u(x,t)=\frac{\int\limits_{0}^{\infty} e^{- \zeta} e^{- \frac{\left(x - \zeta\right)^{2}}{4 a^{2} t}}\, d\zeta}{2 \sqrt{\pi} a \sqrt{t}}`$

$` u(x,t)=\frac{\left(- \pi a \sqrt{t} \left(1 - \frac{x}{2 a^{2} t}\right) e^{a^{2} t \left(1 - \frac{x}{2 a^{2} t}\right)^{2}} \operatorname{erf}{\left(a \sqrt{t} \left(1 - \frac{x}{2 a^{2} t}\right) \right)} + \pi a \sqrt{t} \left(1 - \frac{x}{2 a^{2} t}\right) e^{a^{2} t \left(1 - \frac{x}{2 a^{2} t}\right)^{2}}\right) e^{- \frac{x^{2}}{4 a^{2} t}}}{2 \pi a \sqrt{t} \left(1 - \frac{x}{2 a^{2} t}\right)}`$

$` u(x,t)=\frac{\left(1 - \operatorname{erf}{\left(\frac{2 a^{2} t - x}{2 a \sqrt{t}} \right)}\right) e^{a^{2} t - x}}{2} = \frac{e^{a^{2} t - x} \operatorname{erfc}{\left(\frac{2 a^{2} t - x}{2 a \sqrt{t}} \right)}}{2}`$

---

**15. Given the boundary-value problem** 
```math 
 u_{xx}=a^{-2}u_t, \quad 0<x<\infty, t>0 
 ``` 
```math 
 B.C.: \quad u(0,t)=0, \quad u(x,t) \xrightarrow{} 0 \quad as \quad x \xrightarrow{} \infty 
 ``` 
```math 
 I.C.: \quad u(x,0)=T_0 xe^{\frac{-x^2}{4}}, \quad 0<x<\infty 
 ``` 
 

**Show that** 

```math 
 u(x,t)=T_0 x(1+a^2 t)^{\frac{-3}{2}}exp[\frac{-x^2}{4(1+a^2 t)}] 
 ``` 
 ***Hint***: See Prob. 7(b).

---

**Solution:**

$` From \quad (3.42) \quad we \quad know \quad that:`$

$` U a^{2} s^{2} + U_{t} = 0, \quad t>0`$

$` U(s,0)=F(s), \quad 0<s<\infty`$

$` f(x) = T_{0} x e^{- \frac{x^{2}}{4}}`$

$` U(s,t)=F{\left(s \right)} e^{- a^{2} s^{2} t}`$

$` َFrom \quad (7.b), \quad and \quad using \quad Fourier \quad Sine \quad Transform \quad we \quad can \quad write:`$

$` F(s) = \frac{\sqrt{2} \int\limits_{0}^{\infty} f{\left(\zeta \right)} \sin{\left(s \zeta \right)}\, d\zeta}{\sqrt{\pi}} = \frac{\sqrt{2} \int\limits_{0}^{\infty} T_{0} x e^{- \frac{x^{2}}{4}} \sin{\left(s x \right)}\, dx}{\sqrt{\pi}} = 2 \sqrt{2} T_{0} s e^{- s^{2}}`$

$` u(x,t) = \frac{\sqrt{2} \int\limits_{0}^{\infty} 2 \sqrt{2} T_{0} s e^{- s^{2}} e^{- a^{2} s^{2} t} \sin{\left(s x \right)}\, ds}{\sqrt{\pi}} = T_{0} x \left(\frac{1}{a^{2} t + 1}\right)^{\frac{3}{2}} e^{- \frac{x^{2}}{4 \left(a^{2} t + 1\right)}}`$

---

##### EXERCISES 3.4  

**11. Solve the problem described by (3.76) when**  

$` (a) \quad f(x)=\begin{cases}    F_0, & 0<x<\infty     \\ 0, & -\infty<x<0 \end{cases} `$  

$` (b) \quad f(x)=\begin{cases}    F_0, & |x|<1     \\ 0, & |x|>1 \end{cases} `$

---

**Solution:**

$` From \quad (3.76) \quad and \quad (3.81) \quad we \quad know \quad that:`$

$` u_{tt} + u_{xxxx} = 0, \quad -\infty<x<\infty \quad t>0`$

$` u(x,0) = f{\left(x \right)}, \quad -\infty<x<\infty`$

$` U s^{4} + U_{tt} = 0, \quad t>0`$

$` U(s,0) = F(s)`$

$` U(s,t) = F{\left(s \right)} \cos{\left(s^{2} t \right)}`$

$` u(x,t) = \frac{\sqrt{2} \int\limits_{-\infty}^{\infty} \left(\sin{\left(\frac{\zeta^{2}}{4 t} \right)} + \cos{\left(\frac{\zeta^{2}}{4 t} \right)}\right) f{\left(x - \zeta \right)}\, d\zeta}{4 \sqrt{\pi} \sqrt{t}}`$

$` َSo, \quad replacing \quad f(x-\zeta) \quad with \quad F_0 \quad for \quad a \quad and \quad b:`$

$` u_a(x,t) = \frac{\sqrt{2} \int\limits_{0}^{\infty} F_{0} \left(\sin{\left(\frac{\zeta^{2}}{4 t} \right)} + \cos{\left(\frac{\zeta^{2}}{4 t} \right)}\right)\, d\zeta}{4 \sqrt{\pi} \sqrt{t}} = \frac{F_{0}}{2}`$

$` u_b(x,t) = \frac{\sqrt{2} \int\limits_{-1}^{1} F_{0} \left(\sin{\left(\frac{\zeta^{2}}{4 t} \right)} + \cos{\left(\frac{\zeta^{2}}{4 t} \right)}\right)\, d\zeta}{4 \sqrt{\pi} \sqrt{t}} = F_{0} \left(C\left(\frac{\sqrt{2}}{2 \sqrt{\pi} \sqrt{t}}\right) + S\left(\frac{\sqrt{2}}{2 \sqrt{\pi} \sqrt{t}}\right)\right)`$

---

##### EXERCISES 3.5  

**8. Use the Fourier sine transform to show that**
```math 
 u_{xx}+u_{yy}=0, \quad 0<x<\infty, y>0 
 ``` 
```math 
B.C.: \quad \begin{cases}    u(0,y)=0, & u(x,0)=f(x)     \\ u(x,y) \xrightarrow{} 0 & as \quad (x^2+y^2) \xrightarrow{} \infty \end{cases} 
 ``` 
**Has the formal solution**  

$` (a) \quad u(x,y)=\frac{y}{\pi} \int_{0}^{\infty}f(t)[\frac{1}{(t-x)^2+y^2}-\frac{1}{(t+x)^2+y^2}]dt `$  

(b) **Show that when f(x)=1, the solution in (a) reduces to** $` u(x,y)=\frac{2}{\pi}tan^{-1}(\frac{x}{y})`$

---

**Solution:**

$` From \quad the \quad Dirichlet \quad problem \quad (3.86) \quad we \quad know \quad that:`$

$` u_{xx} + u_{yy} = 0, \quad -\infty<x<\infty \quad y>0`$

$` u(x,0) = f{\left(x \right)}, \quad -\infty<x<\infty`$

$` Since \quad both \quad x \quad and \quad y \quad have \quad a \quad semiinfinite \quad range \quad we \quad use \quad Fourier \quad Sine \quad Transform \quad for \quad both \quad variables:`$

$` U_{yy} - s^{2} \mathcal{SIN}_{x}\left[u{\left(x,y \right)}\right]\left(s\right) + \frac{\sqrt{2} s u{\left(0,y \right)}}{\sqrt{\pi}} = 0, \quad s,y>0`$

$` U_{yy} - s^{2} \mathcal{SIN}_{x}\left[u{\left(x,y \right)}\right]\left(s\right) = 0, \quad s,y>0`$

$` U(s,0) = F_s(s), \quad 0<s<\infty`$

$` Which \quad has \quad the \quad solution:`$

$` U(s,y) = \left(e^{- s y} + e^{- -s y}\right) \mathcal{SIN}_{x}\left[f{\left(x \right)}\right]\left(s\right)`$

$` We \quad know \quad that:`$

$` \mathcal{COS}^{-1}_{-s}\left[e^{- -s y}\right]\left(-x\right) + \mathcal{COS}^{-1}_{s}\left[e^{- s y}\right]\left(x\right) = \frac{\sqrt{2} y}{\sqrt{\pi} \left(x^{2} + y^{2}\right)} + \frac{\sqrt{2} y}{\sqrt{\pi} \left(-x^{2} + y^{2}\right)}`$

$` So, \quad by \quad applying \quad the \quad convolution \quad theorem \quad we \quad can \quad write:`$

$` u(x,y) = \frac{\sqrt{2} \int\limits_{0}^{\infty} \left(\frac{\sqrt{2} y}{\sqrt{\pi} \left(y^{2} + \left(t + x\right)^{2}\right)} + \frac{\sqrt{2} y}{\sqrt{\pi} \left(y^{2} + \left(t - x\right)^{2}\right)}\right) f{\left(t \right)}\, dt}{2 \sqrt{\pi}}`$


$` Now, \quad assuming \quad f(x)=1, \quad and \quad applying \quad the \quad convolution \quad theorem \quad considering \quad f(x)=f(t-x):`$

$` u(x,y) = \frac{\sqrt{2} \int\limits_{0}^{x} \frac{2 \sqrt{2} y}{\sqrt{\pi} \left(x^{2} + y^{2}\right)}\, dx}{2 \sqrt{\pi}} = \frac{2 \operatorname{atan}{\left(\frac{x}{y} \right)}}{\pi}`$

---

##### EXERCISES 3.7  

**1. Use the convolution theorem to show that Eqs. (3.172)-(3.174) can be expressed in the alternate form**
```math 
 \sigma_{xx}=-\frac{2x^3}{\pi} \int_{-\infty}^{\infty} \frac{p(y-\eta)}{(x^2+\eta^2)^2}d\eta 
 ``` 
```math 
 \sigma_{yy}=-\frac{2x}{\pi} \int_{-\infty}^{\infty} \frac{\eta^2p(y-\eta)}{(x^2+\eta^2)^2}d\eta 
 ``` 
```math 
 \sigma_{xy}=-\frac{2x^2}{\pi} \int_{-\infty}^{\infty} \frac{\eta p(y-\eta)}{(x^2+\eta^2)^2}d\eta 
 ``` 
---

**Solution:**

$` Knowing \quad that:`$

$` \sigma_{xx}=\frac{\partial^{2}}{\partial y^{2}} X{\left(x,y \right)}, \sigma_{yy}=\frac{\partial^{2}}{\partial x^{2}} X{\left(x,y \right)}, \sigma_{xy}=- \frac{\partial^{2}}{\partial y\partial x} X{\left(x,y \right)}`$

$` \frac{\partial^{4}}{\partial x^{4}} X{\left(x,y \right)} + \frac{\partial^{4}}{\partial y^{4}} X{\left(x,y \right)} + 2 \frac{\partial^{4}}{\partial y^{2}\partial x^{2}} X{\left(x,y \right)} = 0`$

$` Considering:`$

$` \mathcal{F}_{y}\left[X{\left(x,y \right)}\right]\left(s\right)=G{\left(x,s \right)}`$

$` s^{4} G{\left(x,s \right)} - 2 s^{2} \frac{\partial^{2}}{\partial x^{2}} G{\left(x,s \right)} + \frac{\partial^{4}}{\partial x^{4}} G{\left(x,s \right)} = 0`$

$` s^{2} G{\left(0,s \right)}=P{\left(s \right)},- i s \frac{d}{d x} G{\left(0,s \right)}=0`$

$` Which \quad yields:`$

$` G{\left(x,s \right)}=\frac{\left(x \left|{s}\right| + 1\right) P{\left(s \right)} e^{- x \left|{s}\right|}}{s^{2}}`$

$` X{\left(x,y \right)}=\frac{\sqrt{2} \int\limits_{-\infty}^{\infty} \frac{\left(x \left|{s}\right| + 1\right) P{\left(s \right)} e^{- x \left|{s}\right|} e^{- i s y}}{s^{2}}\, ds}{2 \sqrt{\pi}}`$

$` So, \quad by \quad applying \quad the \quad convolution \quad theorem \quad we \quad can \quad write:`$

$` X{\left(x,y \right)}=- \frac{\sqrt{2} \int\limits_{-\infty}^{\infty} g{\left(\eta \right)} p{\left(- \eta + y \right)}\, d\eta}{2 \sqrt{\pi}}`$

$` g_{xx}(s)=\mathcal{COS}^{-1}_{s}\left[\left(x \left|{s}\right| + 1\right) e^{- x \left|{s}\right|}\right]\left(\eta\right)=\frac{2 \sqrt{2} x^{3}}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}`$

$` \sigma_{xx}=- \frac{\sqrt{2} \int\limits_{-\infty}^{\infty} \frac{2 \sqrt{2} x^{3} p{\left(- \eta + y \right)}}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}\, d\eta}{2 \sqrt{\pi}}`$

$` g_{yy}(s)=\mathcal{COS}^{-1}_{s}\left[\left(- x \left|{s}\right| + 1\right) e^{- x \left|{s}\right|}\right]\left(\eta\right)=\frac{2 \sqrt{2} \eta^{2} x}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}`$

$` \sigma_{yy}=- \frac{\sqrt{2} \int\limits_{-\infty}^{\infty} \frac{2 \sqrt{2} \eta^{2} x p{\left(- \eta + y \right)}}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}\, d\eta}{2 \sqrt{\pi}}`$

$` g_{xy}(s)=\mathcal{SIN}^{-1}_{s}\left[i s x e^{- x \left|{s}\right|}\right]\left(\eta\right)=\frac{2 \sqrt{2} i \eta x^{2}}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}`$


$` \sigma_{xy}=- \frac{\sqrt{2} \int\limits_{-\infty}^{\infty} \frac{2 \sqrt{2} i \eta x^{2} p{\left(- \eta + y \right)}}{\sqrt{\pi} \left(\eta^{2} + x^{2}\right)^{2}}\, d\eta}{2 \sqrt{\pi}}`$

