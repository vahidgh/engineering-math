# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi
  
##### Solutions by Vahid Ghayoomie

# **Chapter 4**

### *THE LAPLACE TRANSFORM*

---

##### EXERCISES 4.2

**In Probs. 1-10, evaluate the Laplace transform of each function directly from the defining integral.**

$` 6. \quad f(t)=te^{2t}`$ 

---

**Solution:**

$` \mathcal{L}_{t}\left[t e^{2 t}\right]\left(p\right)=\int\limits_{0}^{\infty} t e^{2 t} e^{- p t}\, dt=\int\limits_{0}^{\infty} t e^{t \left(2 - p\right)}\, dt`$ 

$` \mathcal{L}_{t}\left[t e^{2 t}\right]\left(p\right)=\begin{cases} \frac{1}{\left(p - 2\right)^{2}} & \text{for}\: \cos{\left(\left|{\arg{\left(p \right)}}\right| \right)} \left|{p}\right| - 2 > 0 \wedge \left|{\arg{\left(p \right)}}\right| < \frac{\pi}{2} \wedge p \neq 2 \\\int\limits_{0}^{\infty} t e^{t \left(2 - p\right)}\, dt & \text{otherwise} \end{cases}`$ 

$` \mathcal{L}_{t}\left[t e^{2 t}\right]\left(p\right)=\left( \frac{1}{\left(p - 2\right)^{2}}, \  2, \  \frac{p}{2} \neq 1\right)`$ 

---

**In Probs. 21-26, verify the Laplace transform relations.**

$` 25. \quad \mathcal{L\{(t+a)^{-\frac{3}{2}};p}\}=\frac{2}{\sqrt{a}} - 2\sqrt{\pi p} e^{ap} erfc(\sqrt{ap}), \quad a>0 `$ 

---

$` Knowing \quad that:`$ 

$` \mathcal{L}_{t}\left[\left(a + t\right)^{-1.5}\right]\left(p\right)=\int\limits_{0}^{\infty} \frac{e^{- p t}}{\left(a + t\right)^{1.5}}\, dt`$ 

$` Considering \quad u=\sqrt{p \left(a + t\right)}`$ 

$` \mathcal{L}_{t}\left[\left(a + t\right)^{-1.5}\right]\left(p\right)=\int\limits_{\sqrt{a} \sqrt{p}}^{\infty \sqrt{p}} \frac{2 u e^{- p \left(- a + \frac{u^{2}}{p}\right)}}{p \left(\frac{u^{2}}{p}\right)^{1.5}}\, du=\int\limits_{\sqrt{a} \sqrt{p}}^{\infty \sqrt{p}} \frac{2 u e^{- u^{2}} e^{a p}}{p \left(\frac{u^{2}}{p}\right)^{1.5}}\, du`$ 

$` Knowing \quad that:`$ 

$` \int\limits_{\sqrt{a} \sqrt{p}}^{\infty} \frac{e^{- u^{2}}}{u^{2}}\, du=\sqrt{\pi} \operatorname{erf}{\left(\sqrt{a} \sqrt{p} \right)} - \sqrt{\pi} + \frac{e^{- a p}}{\sqrt{a} \sqrt{p}}=- \sqrt{\pi} \operatorname{erfc}{\left(\sqrt{a} \sqrt{p} \right)} + \frac{e^{- a p}}{\sqrt{a} \sqrt{p}}`$ 

$` We \quad can \quad write:`$ 

$` \mathcal{L}_{t}\left[\left(a + t\right)^{-1.5}\right]\left(p\right)=2 \sqrt{p} \left(- \sqrt{\pi} \operatorname{erfc}{\left(\sqrt{a} \sqrt{p} \right)} + \frac{e^{- a p}}{\sqrt{a} \sqrt{p}}\right) e^{a p}=- 2 \sqrt{\pi} \sqrt{p} e^{a p} \operatorname{erfc}{\left(\sqrt{a} \sqrt{p} \right)} + \frac{2}{\sqrt{a}}`$ 

---

##### EXERCISES 4.3

**3. Given that $` \mathcal{L\{(\sin t)/t;p\}} = tan^{-1}(1/p) `$, find the Laplace transform of $` (\sin at)/t `$ where $` a>0 `$.**

---

**Solution:**

$` \mathcal{L}_{t}\left[\frac{(1/a) a \sin{\left(a t \right)}}{t}\right]\left(p\right)=\int\limits_{0}^{\infty} \frac{(1/a) a e^{- p t} \sin{\left(a t \right)}}{t}\, dt`$ 

$` Considering \quad u=a t`$ 

$` \mathcal{L}_{u}\left[\frac{(1/a) a^{2} \sin{\left(u \right)}}{u}\right]\left(p\right)=\int\limits_{0}^{\infty a} \frac{(1/a) a e^{- \frac{p u}{a}} \sin{\left(u \right)}}{u}\, du=\mathcal{L}_{u}\left[\frac{\sin{\left(u \right)}}{u}\right]\left(\frac{p}{a}\right)`$ 

$` And \quad also \quad knowing \quad that:`$ 

$` \mathcal{L}_{t}\left[a f{\left(a t \right)}\right]\left(p\right)=(1/a) a \mathcal{L}_{t}\left[f{\left(t \right)}\right]\left(\frac{p}{a}\right)`$ 

$` We \quad can \quad conclude:`$ 

$` \mathcal{L}_{t}\left[\frac{(1/a) a \sin{\left(a t \right)}}{t}\right]\left(p\right)=\left( \operatorname{atan}{\left(\frac{a}{p} \right)}, \  0, \  2 \left|{\arg{\left(a \right)}}\right| = 0\right)`$ 

---

**22. Given that** $` \mathcal{L\{\sin \sqrt t;p\}} = (1/2p) \sqrt{\pi / p} \, e^{-1/4p} `$,

**(a) use Eq. (4.11) to determine the transform of** $` (1/\sqrt t) \cos \sqrt t `$.

**(b) use Theor. 4.9 to determine the transform of** $` (1/t) \sin \sqrt t `$.

---

**Solution:**

$` a) \quad Knowing \quad that:`$ 

$` \mathcal{L}_{t}\left[\frac{d}{d t} f{\left(t \right)}\right]\left(p\right)=p F{\left(p \right)} - f{\left(0 \right)}`$ 

$` \frac{d}{d t} 2 \sin{\left(\sqrt{t} \right)}=\frac{\cos{\left(\sqrt{t} \right)}}{\sqrt{t}}`$ 

$` We \quad can \quad conclude:`$ 

$` \mathcal{L}_{t}\left[\frac{\cos{\left(\sqrt{t} \right)}}{\sqrt{t}}\right]\left(p\right)=\sqrt{\pi} \sqrt{\frac{1}{p}} e^{- \frac{1}{4 p}}=\left( \frac{\sqrt{\pi} e^{- \frac{1}{4 p}}}{\sqrt{p}}, \  0, \  \text{True}\right)`$ 


$` b) \quad Knowing \quad that:`$ 

$` \mathcal{L}_{t}\left[\frac{f{\left(t \right)}}{t}\right]\left(p\right)=\int\limits_{p}^{\infty} F{\left(u \right)}\, du`$ 

$` \mathcal{L}_{t}\left[\frac{\sin{\left(\sqrt{t} \right)}}{t}\right]\left(p\right)=\int\limits_{p}^{\infty} \frac{\sqrt{\pi} \sqrt{\frac{1}{u}} e^{- \frac{1}{4 u}}}{2 u}\, du`$ 

$` We \quad can \quad conclude:`$ 

$` \mathcal{L}_{t}\left[\frac{\sin{\left(\sqrt{t} \right)}}{t}\right]\left(p\right)=- \pi \operatorname{erfc}{\left(\frac{\sqrt{\frac{1}{p}}}{2} \right)} + \pi=\left( \pi \operatorname{erf}{\left(\frac{1}{2 \sqrt{p}} \right)}, \  0, \  \text{True}\right)`$ 

---

**Solution:**

**33. The *Laguerre polynomials* are defined by**
```math 
L_n(t) = \frac{e^t}{n!}\frac{d^n}{dt^n}(t^n e^{-t}) , \quad n = 0,1,2,... 
 ``` 

**Show that**
```math 
\mathcal{L}\{L_n(t);p\} = \frac{1}{p}\left(\frac{p-1}{p}\right)^n 
 ``` 

---

**Solution:**

$` L_{n}\left(t\right)=\frac{e^{t} \frac{\partial^{n}}{\partial t^{n}} t^{n} e^{- t}}{n!}`$ 

$` Knowing \quad that:`$ 

$` \mathcal{L}_{t}\left[\frac{d^{n}}{d t^{n}} f{\left(t \right)}\right]\left(p\right)=p^{n} F{\left(p \right)} - \sum_{k=1}^{n} p^{k - 1} \frac{d^{- k + n}}{d t^{- k + n}} f{\left(0 \right)}`$ 

$` Since \quad for \quad t=0:`$ 

$` f{\left(t^{n} e^{- t} \right)}=0^{n}`$ 

$` t e^{- t}=t e^{- t}=0`$ 

$` \frac{d}{d t} t^{2} e^{- t}=- t^{2} e^{- t} + 2 t e^{- t}=0`$ 

$` \frac{d^{2}}{d t^{2}} t^{3} e^{- t}=t \left(t^{2} - 6 t + 6\right) e^{- t}=0`$ 

$` ...`$ 


$` \frac{\partial^{n - 1}}{\partial t^{n - 1}} t^{n} e^{- t}=0`$ 


$` So:`$ 

$` \mathcal{L}_{t}\left[\frac{d^{n}}{d t^{n}} f{\left(t \right)}\right]\left(p\right)=p^{n} F{\left(p \right)}`$ 



$` Now \quad considering:`$ 

$` \mathcal{L}_{t}\left[t^{n} e^{- t}\right]\left(p\right)=\left( \left(p + 1\right)^{- n - 1} \Gamma\left(n + 1\right), \  0, \  \operatorname{re}{\left(n\right)} + 1 > 0\right)`$ 

$` \mathcal{L}_{t}\left[\frac{t^{n} e^{- t}}{n!}\right]\left(p\right)=\left( \left(p + 1\right)^{- n - 1}, \  0, \  \operatorname{re}{\left(n\right)} + 1 > 0\right)`$ 

$` \mathcal{L}_{t}\left[\frac{\partial^{n}}{\partial t^{n}} \frac{t^{n} e^{- t}}{n!}\right]\left(p\right)=p^{n} \left(p + 1\right)^{- n - 1}`$ 


$` Finally \quad shifting \quad expression:`$ 

$` \mathcal{L}_{t}\left[e^{t} \frac{\partial^{n}}{\partial t^{n}} \frac{t^{n} e^{- t}}{n!}\right]\left(p\right)=p^{- n - 1} \left(p - 1\right)^{n}`$ 

---

##### EXERCISES 4.4

**In Probs. 9-15, use infinite series to derive the given Laplace transform relation.**

$` 12. \quad \mathcal{L\{erfc(1/\sqrt t);p}\}= (1/p)e^{-2\sqrt p} `$ 

---

**Solution:**

$` Knowing \quad that:`$ 

$` \operatorname{erfc}{\left(z \right)}=\frac{2 \int\limits_{z}^{\infty} \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{2 n}}{n!}\, dt}{\sqrt{\pi}}`$ 

$` \operatorname{erfc}{\left(\frac{1}{\sqrt{t}} \right)}=\frac{2 \int\limits_{\frac{1}{\sqrt{t}}}^{\infty} \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{2 n}}{n!}\, dt}{\sqrt{\pi}}=\frac{2 \int\limits_{t}^{0} \left(- \frac{\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{- n}}{n!}}{2 t^{\frac{3}{2}}}\right)\, dt}{\sqrt{\pi}}`$ 

$` \operatorname{erfc}{\left(\frac{1}{\sqrt{t}} \right)}=- \frac{2 \int\limits_{0}^{t} \left(- \frac{\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{- n}}{n!}}{2 t^{\frac{3}{2}}}\right)\, dt}{\sqrt{\pi}}`$ 

$` \frac{\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{- n}}{n!}}{\sqrt{\pi} t^{\frac{3}{2}}}=\frac{e^{- \frac{1}{t}}}{\sqrt{\pi} t^{\frac{3}{2}}}`$ 


$` From \quad Laplace \quad properties \quad we \quad know:`$ 

$` \mathcal{L}_{t}\left[\int\limits_{0}^{t} f{\left(t \right)}\, dt\right]\left(p\right)=\frac{\mathcal{L}_{t}\left[f{\left(t \right)}\right]\left(p\right)}{p}`$ 

$` Now \quad we \quad can \quad write:`$ 

$` \mathcal{L}_{t}\left[\frac{e^{- \frac{1}{t}}}{\sqrt{\pi} t^{\frac{3}{2}}}\right]\left(p\right)=\left( e^{- 2 \sqrt{p}}, \  0, \  \text{True}\right)`$ 

$` \mathcal{L}_{t}\left[\operatorname{erfc}{\left(\frac{1}{\sqrt{t}} \right)}\right]\left(p\right)=\frac{e^{- 2 \sqrt{p}}}{p}`$ 

---

**Solution:**

$` Second \quad method \quad with \quad Cosine \quad result:`$ 

$` \mathcal{L}_{t}\left[- \frac{2 \int\limits_{0}^{t} \left(- \frac{\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{- n}}{n!}}{2 t^{\frac{3}{2}}}\right)\, dt}{\sqrt{\pi}}\right]\left(p\right)=\frac{\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} \mathcal{L}_{t}\left[\frac{t^{- n}}{t^{1.5}}\right]\left(p\right)}{n!}}{\sqrt{\pi} p}`$ 

$` \mathcal{L}_{t}\left[\frac{t^{- n}}{t^{1.5}}\right]\left(p\right)=\left( p^{n + 0.5} \Gamma\left(- n - 0.5\right), \  0, \  \operatorname{re}{\left(n\right)} + 1.5 < 1\right)`$ 

$` From \quad Gamma \quad properties \quad we \quad know:`$ 

$` \Gamma\left(- n - 0.5\right) \Gamma\left(n + 1.5\right)=\frac{\pi}{\sin{\left(\pi n + 1.5 \pi \right)}}=- \frac{\pi}{\cos{\left(\pi n \right)}}=- \left(-1\right)^{- 2 n} \pi`$ 

$` \Gamma\left(n + 1.5\right)=\frac{2^{- 2 n} \sqrt{\pi} \left(2 n\right)!}{n!}`$ 

$` \Gamma\left(- n - 0.5\right)=- \frac{\left(-1\right)^{- 2 n} 2^{2 n} \sqrt{\pi} n!}{\left(2 n\right)!}`$ 

$` So, \quad we \quad conclude:`$ 

$` \frac{\sum_{n=0}^{\infty} - \frac{\left(-1\right)^{- n} 2^{2 n} \sqrt{\pi} p^{n + 0.5}}{\left(2 n\right)!}}{\sqrt{\pi} p}=- \frac{\cos{\left(2 \sqrt{p} \right)}}{p^{0.5}}`$ 


$` Now \quad we \quad can \quad write:`$ 

$` \mathcal{L}_{t}\left[\operatorname{erfc}{\left(\frac{1}{\sqrt{t}} \right)}\right]\left(p\right)=- \frac{\cos{\left(2 \sqrt{p} \right)}}{p^{0.5}}`$ 

---

**Solution:**

$` Third \quad method \quad with \quad Sine \quad result:`$ 


$` Knowing \quad that:`$ 

$` \operatorname{erfc}{\left(\frac{1}{\sqrt{t}} \right)}=- \frac{2 \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{- n - \frac{1}{2}}}{\left(2 n + 1\right) n!}}{\sqrt{\pi}} + 1`$ 

$` \mathcal{L}_{t}\left[1 - \operatorname{erf}{\left(\frac{1}{\sqrt{t}} \right)}\right]\left(p\right)=\mathcal{L}_{t}\left[1\right]\left(p\right) - \frac{2 \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} \mathcal{L}_{t}\left[t^{- n - \frac{1}{2}}\right]\left(p\right)}{\left(2 n + 1\right) n!}}{\sqrt{\pi}}`$ 

$` \mathcal{L}_{t}\left[1\right]\left(p\right)=\left( \frac{1}{p}, \  0, \  \text{True}\right)`$ 

$` \mathcal{L}_{t}\left[t^{- n - \frac{1}{2}}\right]\left(p\right)=\left( p^{n - \frac{1}{2}} \Gamma\left(\frac{1}{2} - n\right), \  0, \  \operatorname{re}{\left(n\right)} + \frac{1}{2} < 1\right)`$ 


$` We \quad can \quad write:`$ 

$` \mathcal{L}_{t}\left[1 - \operatorname{erf}{\left(\frac{1}{\sqrt{t}} \right)}\right]\left(p\right)=- \frac{2 \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} p^{n - \frac{1}{2}} \Gamma\left(\frac{1}{2} - n\right)}{\left(2 n + 1\right) n!}}{\sqrt{\pi}} + \frac{1}{p}`$ 


$` From \quad Gamma \quad properties \quad we \quad know:`$ 

$` \Gamma\left(0.5 - n\right) \Gamma\left(n + 0.5\right)=\frac{\pi}{\sin{\left(\pi \left(n + 0.5\right) \right)}}=\frac{\pi}{\cos{\left(\pi n \right)}}=\left(-1\right)^{- 2 n} \pi`$ 

$` \Gamma\left(n + 0.5\right)=\frac{2^{- 2 n} \sqrt{\pi} \left(2 n\right)!}{n!}`$ 

$` \Gamma\left(0.5 - n\right)=\frac{\left(-1\right)^{- 2 n} 2^{2 n} \sqrt{\pi} n!}{\left(2 n\right)!}`$ 


$` Now \quad we \quad can \quad write:`$ 

$` \mathcal{L}_{t}\left[1 - \operatorname{erf}{\left(\frac{1}{\sqrt{t}} \right)}\right]\left(p\right)=- \frac{2 \sum_{n=0}^{\infty} \frac{\left(-1\right)^{- n} 2^{2 n} \sqrt{\pi} p^{n - \frac{1}{2}}}{\left(2 n + 1\right) \left(2 n\right)!}}{\sqrt{\pi}} + \frac{1}{p}=- \frac{\sin{\left(2 \sqrt{p} \right)}}{p} + \frac{1}{p}`$ 

---

**23. Show that**

(a) $`\mathcal{L\{J_0(t)\sin t;p}\}= \frac{1}{\sqrt p \sqrt[4]{p^2+4}}\sin(\frac{1}{2}tan^{-1}\frac{2}{p}) `$ 

(b) $`\mathcal{L\{J_0(t)\cos t;p}\}= \frac{1}{\sqrt p \sqrt[4]{p^2+4}}\cos(\frac{1}{2}tan^{-1}\frac{2}{p}) `$ 

---

**Solution:**

$` \mathcal{L}_{t}\left[\sin{\left(t \right)} J_{0}\left(t\right)\right]\left(p\right)=\frac{\sin{\left(0.5 \operatorname{atan}{\left(\frac{2}{p} \right)} \right)}}{\sqrt{p} \sqrt[4]{p^{2} + 4}}`$ 

$` \mathcal{L}_{t}\left[J_{0}\left(t\right)\right]\left(p\right)=\frac{1}{\sqrt{p^{2} + 1}}`$ 

$` \mathcal{L}_{t}\left[\sin{\left(t \right)}\right]\left(p\right)=\frac{1}{p^{2} + 1}`$ 

$` \sin{\left(t \right)} J_{0}\left(t\right)=J_{0}\left(t\right) \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} t^{2 n + 1}}{\left(2 n + 1\right)!}`$ 

$` \mathcal{L}_{t}\left[\sin{\left(t \right)} J_{0}\left(t\right)\right]\left(p\right)=\sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} \mathcal{L}_{t}\left[t^{2 n + 1} J_{0}\left(t\right)\right]\left(p\right)}{\left(2 n + 1\right)!}`$ 

$` From \quad Ex.4.16 \quad we \quad know \quad that:`$ 

$` t^{2 n + 1} J_{0}\left(t\right)=t^{2 n + 1} \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} \left(\frac{t}{2}\right)^{2 n}}{n! \Gamma\left(n + 1\right)}`$ 

$` t^{2 n + 1} \left(\frac{t}{2}\right)^{2 n}=4^{- n} t^{4 n + 1}`$ 

$` \mathcal{L}_{t}\left[4^{- n} t^{4 n + 1}\right]\left(p\right)=4^{- n} p^{- 4 n - 2} \Gamma\left(4 n + 2\right)`$ 

$` t^{2 n + 1} J_{0}\left(t\right)=t^{2 n + 1} \sum_{n=0}^{\infty} \frac{\left(-1\right)^{n} \left(\frac{t}{2}\right)^{2 n}}{n! \Gamma\left(n + 1\right)}`$ 


$` From \quad Probs.4.17-4.21 \quad we \quad can \quad conclude:`$ 

$` \mathcal{L}_{t}\left[t^{2 n + 1} J_{0}\left(t\right)\right]\left(p\right)=\frac{2^{- n} p \left(p^{2} + 1\right)^{- 2 n - 1.5} \left(3 n + 1\right)!}{n!}`$ 


*Maybe it's also possible to find a better solution by expanding J_0(t) instead of sin(t)!*

---

##### EXERCISES 4.5

**In Probs. 1-10, determine the inverse Laplace transform using the table in Appendix C and various operational properties.**

$` 10. \quad F(p)=\frac{(p+1)e^{-\pi p}}{p^2+p+1}`$ 

---

**Solution:**

$` By \quad partial \quad fraction \quad we \quad can \quad write:`$ 

$` \frac{p + 1}{p^{2} + p + 1}=\frac{\frac{1}{2} + \frac{\sqrt{3} i}{6}}{p + \frac{1}{2} + \frac{\sqrt{3} i}{2}} + \frac{\frac{1}{2} - \frac{\sqrt{3} i}{6}}{p + \frac{1}{2} - \frac{\sqrt{3} i}{2}}`$ 

$` Calculating \quad Inverse \quad Laplace \quad Transform \quad of \quad each \quad function:`$ 

$` \mathcal{L}^{-1}_{p}\left[\frac{\frac{1}{2} + \frac{\sqrt{3} i}{6}}{p + \frac{1}{2} + \frac{\sqrt{3} i}{2}} + \frac{\frac{1}{2} - \frac{\sqrt{3} i}{6}}{p + \frac{1}{2} - \frac{\sqrt{3} i}{2}}\right]\left(t\right)=\frac{\left(\sqrt{3} \sin{\left(\frac{\sqrt{3} t}{2} \right)} + 3 \cos{\left(\frac{\sqrt{3} t}{2} \right)}\right) e^{- \frac{t}{2}}}{3}`$ 

$` \mathcal{L}^{-1}_{p}\left[e^{- \pi p}\right]\left(t\right)=\delta\left(t - \pi\right)`$ 

$` Finally, \quad by \quad applying \quad the \quad convolution \quad theorem \quad we \quad can \quad write:`$ 

$` \int\limits_{0}^{t} \frac{\left(\sqrt{3} \sin{\left(\frac{\sqrt{3} \left(t - u\right)}{2} \right)} + 3 \cos{\left(\frac{\sqrt{3} \left(t - u\right)}{2} \right)}\right) e^{- \frac{t}{2} + \frac{u}{2}} \delta\left(u - \pi\right)}{3}\, du=\frac{\sqrt{3} e^{\frac{\pi}{2}} e^{- \frac{t}{2}} \sin{\left(\frac{\sqrt{3} t}{2} - \frac{\sqrt{3} \pi}{2} \right)} \theta\left(t - \pi\right)}{3} + e^{\frac{\pi}{2}} e^{- \frac{t}{2}} \cos{\left(\frac{\sqrt{3} t}{2} - \frac{\sqrt{3} \pi}{2} \right)} \theta\left(t - \pi\right)`$ 

$` \mathcal{L}^{-1}_{p}\left[\frac{\left(p + 1\right) e^{- \pi p}}{p^{2} + p + 1}\right]\left(t\right)=\frac{\sqrt{3} \left(\sin{\left(\frac{\sqrt{3} \left(t - \pi\right)}{2} \right)} + \sqrt{3} \cos{\left(\frac{\sqrt{3} \left(t - \pi\right)}{2} \right)}\right) e^{- \frac{t}{2} + \frac{\pi}{2}} \theta\left(t - \pi\right)}{3}`$ 

---

**21. Given that $` \mathcal{L^{-1}}\{F(p);t\}=f(t) `$, show for constants, a, b, and k that**

(a) $` \mathcal{L^{-1}}\{F(kp);t\}=(1/k)f(t/k), \quad k>0 `$ 

(b) $` \mathcal{L^{-1}}\{F(ap+b);t\}=(1/a) e^{-bt/a} f(t/a), \quad a>0 `$ 

---

**Solution:**

$` Knowing \quad that:`$ 

$` \mathcal{L}^{-1}_{p}\left[F{\left(k p \right)}\right]\left(t\right)=- \frac{i \int\limits_{c - \infty i}^{c + \infty i} F{\left(k p \right)} e^{p t}\, dp}{2 \pi}`$ 

$` Replacing \quad kp \quad with \quad p:`$ 

$` \int\limits_{c - \infty i}^{c + \infty i} F{\left(k p \right)} e^{p t}\, dp=\int\limits_{k \left(c - \infty i\right)}^{k \left(c + \infty i\right)} \frac{F{\left(p \right)} e^{\frac{p t}{k}}}{k}\, dp`$ 

$` We \quad can \quad write:`$ 

$` \mathcal{L}^{-1}_{p}\left[F{\left(k p \right)}\right]\left(t\right)=\frac{f{\left(\frac{t}{k} \right)}}{k}`$ 


$` For \quad part \quad b:`$ 

$` \mathcal{L}^{-1}_{p}\left[F{\left(a p + b \right)}\right]\left(t\right)=- \frac{i \int\limits_{c - \infty i}^{c + \infty i} F{\left(a p + b \right)} e^{p t}\, dp}{2 \pi}`$ 

$` Replacing \quad ap+b \quad with \quad p:`$ 

$` \int\limits_{c - \infty i}^{c + \infty i} F{\left(a p + b \right)} e^{p t}\, dp=\int\limits_{a \left(c - \infty i\right) + b}^{a \left(c + \infty i\right) + b} \frac{F{\left(p \right)} e^{\frac{t \left(- b + p\right)}{a}}}{a}\, dp=\int\limits_{c a + b - \infty i}^{c a + b + \infty i} \frac{F{\left(p \right)} e^{- \frac{b t}{a}} e^{\frac{p t}{a}}}{a}\, dp`$ 

$` We \quad can \quad write:`$ 

$` \mathcal{L}^{-1}_{p}\left[F{\left(a p + b \right)}\right]\left(t\right)=\frac{f{\left(\frac{t}{a} \right)} e^{- \frac{b t}{a}}}{a}`$ 

---

**40. Show that**

(a) $` \int_{0}^{t}{J_0(t-u)J_0(u)\, du} = \sin{t} `$ 

(b) $` \int_{0}^{t}{\cos{(t-u)}\sin{u}\, du} = (1/2)t\, \sin{t} `$ 

(c) $` \int_{0}^{t}{\sin{(t-u)} J_0(u)\, du} = (1/2)t\, J_1(t) `$ 

(d) $` \int_{0}^{t}{J_1(t-u)J_0(u)\, du} = J_0(t) - \cos(t) `$ 

---

**Solution:**

$` Using \quad the \quad convolution \quad theorem \quad we \quad can \quad write:`$ 

$` (f*g)(t)=\int\limits_{0}^{t} f{\left(u \right)} g{\left(t - u \right)}\, du`$ 

$` \mathcal{L}^{-1}_{p}\left[F{\left(p \right)} G{\left(p \right)}\right]\left(t\right)=(f*g)(t)`$ 


$` For \quad part \quad a:`$ 

$` f{\left(t \right)}=g{\left(t \right)}=J_{0}\left(t\right)`$ 

$` F{\left(p \right)}=G{\left(p \right)}=\mathcal{L}_{t}\left[J_{0}\left(t\right)\right]\left(p\right)=\left( \frac{1}{\sqrt{p^{2} + 1}}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` \int\limits_{0}^{t} J_{0}\left(u\right) J_{0}\left(t - u\right)\, du=\mathcal{L}^{-1}_{p}\left[\frac{1}{p^{2} + 1}\right]\left(t\right)=\sin{\left(t \right)}`$ 


$` For \quad part \quad b:`$ 

$` f{\left(t \right)}=\sin{\left(t \right)}`$ 

$` g{\left(t \right)}=\cos{\left(t \right)}`$ 

$` F{\left(p \right)}=\mathcal{L}_{t}\left[\sin{\left(t \right)}\right]\left(p\right)=\left( \frac{1}{p^{2} + 1}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` G{\left(p \right)}=\mathcal{L}_{t}\left[\cos{\left(t \right)}\right]\left(p\right)=\left( \frac{p}{p^{2} + 1}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` \int\limits_{0}^{t} \sin{\left(u \right)} \cos{\left(t - u \right)}\, du=\mathcal{L}^{-1}_{p}\left[\frac{p}{\left(p^{2} + 1\right)^{2}}\right]\left(t\right)=\frac{t \sin{\left(t \right)}}{2}`$ 


$` For \quad part \quad c:`$ 

$` f{\left(t \right)}=J_{0}\left(t\right)`$ 

$` g{\left(t \right)}=\sin{\left(t \right)}`$ 

$` F{\left(p \right)}=\mathcal{L}_{t}\left[J_{0}\left(t\right)\right]\left(p\right)=\left( \frac{1}{\sqrt{p^{2} + 1}}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` G{\left(p \right)}=\mathcal{L}_{t}\left[\sin{\left(t \right)}\right]\left(p\right)=\left( \frac{1}{p^{2} + 1}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` \int\limits_{0}^{t} \sin{\left(t - u \right)} J_{0}\left(u\right)\, du=\mathcal{L}^{-1}_{p}\left[\frac{1}{\left(p^{2} + 1\right)^{\frac{3}{2}}}\right]\left(t\right)=t J_{1}\left(t\right)`$ 


$` For \quad part \quad d:`$ 

$` f{\left(t \right)}=J_{0}\left(t\right)`$ 

$` g{\left(t \right)}=J_{1}\left(t\right)`$ 

$` F{\left(p \right)}=\mathcal{L}_{t}\left[J_{0}\left(t\right)\right]\left(p\right)=\left( \frac{1}{\sqrt{p^{2} + 1}}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 
$` G{\left(p \right)}=\mathcal{L}_{t}\left[J_{1}\left(t\right)\right]\left(p\right)=\left( 1 - \frac{1}{\sqrt{1 + \frac{1}{p^{2}}}}, \  -\infty, \  \operatorname{re}{\left(p\right)} > 0\right)`$ 

$` \int\limits_{0}^{t} J_{0}\left(u\right) J_{1}\left(t - u\right)\, du=\mathcal{L}^{-1}_{p}\left[\frac{1}{\sqrt{p^{2} + 1}} - \frac{1}{\sqrt{1 + \frac{1}{p^{2}}} \sqrt{p^{2} + 1}}\right]\left(t\right)=- \mathcal{L}^{-1}_{p}\left[\frac{1}{\sqrt{1 + \frac{1}{p^{2}}} \sqrt{p^{2} + 1}}\right]\left(t\right) + J_{0}\left(t\right)`$ 


$` \int\limits_{0}^{t} J_{0}\left(u\right) J_{1}\left(t - u\right)\, du=- \mathcal{L}^{-1}_{p}\left[\frac{p}{p^{2} + 1}\right]\left(t\right) + J_{0}\left(t\right)=- \cos{\left(t \right)} + J_{0}\left(t\right)`$ 

