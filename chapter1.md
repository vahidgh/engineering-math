# **Integral transforms For Engineers**

### Larry C. Andrews, Bhimsen K. Shivamoggi
  
##### Solutions by Vahid Ghayoomie

# **Chapter 1**

### *Special Functions*

---

##### EXERCISES 1.2  

**In Probs. 11-13, verify the given integral formula.**

$`\ 13.\quad \Gamma\left(x\right)=\log{\left(b \right)}^{x} \int\limits_{0}^{\infty} b^{- t} t^{x - 1}\, dt,\quad x>0, b>1`$

---

**Solution:**

$` Considering\quad u=t \log{\left(b \right)}\quad and \quad b=e^{\log b}:`$

$` \Gamma\left(x\right)=\log{\left(b \right)}^{x} \int\limits_{0}^{\infty} \frac{\left(\frac{u}{\log{\left(b \right)}}\right)^{x - 1} e^{- u}}{\log{\left(b \right)}}\, du`$\n
$` And\quad Simplifying:`$

$` \Gamma\left(x\right)=\int\limits_{0}^{\infty} u^{x - 1} e^{- u}\, du`$ 

---

**24. Using the result of Prob. 23, derive the relation**
    
```math 
\Gamma(n+1/2) = \frac{(2n)!}{2^{2n}n!}\sqrt{\pi}, \quad n=0,1,2,...
```

---

**Solution:**

From prob.23 we have: $`sqrt \pi \Gamma(2x) = 2^{2x-1}\Gamma(x)\Gamma(x+1/2)`$ so:  

```math 
Gamma(n+1/2) = \frac{\sqrt{\pi}\Gamma(2n)}{2^{2n-1}\Gamma(n)}=\frac{(2n-1)!}{2^{2n-1}(n-1)!}\sqrt{\pi}=\frac{\frac{(2n)!}{2n}}{2^{2n-1}(\frac{n!}{n})}=\frac{(2n)!}{2^{2n}(n)!}\sqrt{\pi}
``` 

---

##### EXERCISES 1.4

**5. Using the *Jacobi-Anger expansion***

```math 
e^{ixsin\theta} = \sum_{n=-\infty}^{\infty} J_n(x)e^{in\theta}
```

**Show that**

   (a) $`cos(xsin\theta) = J_0(x) + 2 \sum_{n=1}^{\infty} J_{2n}(x)cos(2n\theta)`$ 

   (b) $`sin(xsin\theta) = 2 \sum_{n=1}^{\infty} J_{2n-1}(x)sin[(2n-1)\theta]`$ 

   (c) $`cos(x) = J_0(x) + 2 \sum_{n=1}^{\infty} (-1)^nJ_{2n}(x)`$ 

   (d) $`sin(x) = 2 \sum_{n=1}^{\infty} (-1)^nJ_{2n-1}(x)`$**

---

**Solution:**

$` e^{i x \sin{\left(\theta \right)}}=\sum_{n=-\infty}^{\infty} e^{i n \theta} J_{n}\left(x\right)`$ 

$` Expanding \quad exponential \quad functions:`$ 

$` i \sin{\left(x \sin{\left(\theta \right)} \right)} + \cos{\left(x \sin{\left(\theta \right)} \right)}=\sum_{n=-\infty}^{\infty} \left(i \sin{\left(n \theta \right)} + \cos{\left(n \theta \right)}\right) J_{n}\left(x\right)`$ 

$` Expanding \quad Sum \quad to \quad negative \quad and \quad positive \quad numbers \quad and \quad considering:`$ 

$` J_{-n}(x)=(-1)^nJ_n(x)`$ 

$` We \quad will \quad have \quad two \quad separate \quad equations \quad for \quad even \quad and \quad odd \quad numbers:`$ 

$` i \sin{\left(x \sin{\left(\theta \right)} \right)}=2 i \sum_{n=1}^{\infty} \sin{\left(\theta \left(2 n - 1\right) \right)} J_{2 n - 1}\left(x\right)`$ 

$` \cos{\left(x \sin{\left(\theta \right)} \right)}=J_{0}\left(x\right) + 2 \sum_{n=1}^{\infty} \cos{\left(2 n \theta \right)} J_{2 n}\left(x\right)`$ 

$` And \quad for \quad \theta=\frac{\pi}{2}:`$ 

$` \sin{\left(x \right)}=2 \sum_{n=1}^{\infty} \sin{\left(\pi \left(n - \frac{1}{2}\right) \right)} J_{2 n - 1}\left(x\right)=2 \sum_{n=1}^{\infty} \left(-1\right)^{n} J_{2 n - 1}\left(x\right)`$ 

$` \cos{\left(x \right)}=J_{0}\left(x\right) + 2 \sum_{n=1}^{\infty} \left(-1\right)^{n} J_{2 n}\left(x\right)`$ 

---

16. **Show that** $` I_{-n}(Z)=I_n(Z), \quad n=0,1,2,... `$

---

**Solution:**

$` Knowing \quad that:`$ 

$` I_n(y)=i^{-n}J_n(iy)`$ 

$` J_{-n}(y)=(-1)^nJ_n(y)`$ 

$` We \quad can \quad conclude:`$ 


$` I_{-n}(Z)=i^{n}J_{-n}(iZ)=i^n(-1)^nJ_n(iZ)=(-i)^nJ_n(iZ)=(-i)^ni^nI_n(Z)=(1)^nI_n(Z)=I_n(Z)`$
